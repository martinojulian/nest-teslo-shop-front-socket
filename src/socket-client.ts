import { Manager, Socket } from "socket.io-client";

let socket: Socket;

export const connectToServer = (jwt: string) => {
  const manager = new Manager("http://localhost:3000/socket.io/socket.io.js", {
    extraHeaders: {
      authentication: jwt,
    },
  });

  socket = manager.socket("/");

  socket.removeAllListeners();
  addListeners();
};

const addListeners = () => {
  const serverStatusLabel = document.querySelector("#server-status")!;
  const clientUl = document.querySelector("#clients-ul")!;
  const messageForm = document.querySelector<HTMLFormElement>("#message-form")!;
  const messageInput =
    document.querySelector<HTMLInputElement>("#message-input")!;
  const messagesUl = document.querySelector<HTMLUListElement>("#messages-ul")!;

  socket.on("connect", () => {
    serverStatusLabel.innerHTML = "Conectado";
  });

  socket.on("disconnect", () => {
    serverStatusLabel.innerHTML = "Desconectado";
  });

  socket.on("clients-uploads", (clients: string[]) => {
    let clientesHtml = "";
    clients.forEach((clientId) => {
      clientesHtml += `
        <li>${clientId}</li>
        `;
    });

    clientUl.innerHTML = clientesHtml;
  });

  messageForm.addEventListener("submit", (event) => {
    event.preventDefault();
    if (messageInput.value.trim().length <= 0) return;

    socket.emit("message-from-client", {
      message: messageInput.value.trim(),
    });

    messageInput.value = "";
  });

  socket.on(
    "message-from-server",
    (payload: { fullName: string; message: string }) => {
      const newMessage = `
        <li>
            <strong> ${payload.fullName} </strong> - 
            <span> ${payload.message} </span>
        </li>
      `;
      const li = document.createElement("li");
      li.innerHTML = newMessage;
      messagesUl.append(li);
    }
  );
};
