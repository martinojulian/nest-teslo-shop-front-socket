import "./style.css";
import { connectToServer } from "./socket-client";

document.querySelector<HTMLDivElement>("#app")!.innerHTML = `
  <div>
    <h1> Websocket - Client</h1>

    <input  id="jwt-token" placeholder="Json web tokeb"/>
      <button id="btn-connect"> Conectar </button>
    
      <br>

    <span id="server-status">offline</span>

    <ul id="clients-ul">
    </ul>


    <form id="message-form">
      <input  id="message-input" placeholder="Ingrese mensaje"/>
    </form>


    <h3>MEssages</h3>
    <ul id="messages-ul"></ul>
  </div>
`;

// setupCounter(document.querySelector<HTMLButtonElement>("#counter")!);
// connectToServer();
const jwtToken = document.querySelector<HTMLInputElement>("#jwt-token")!;
const btnConnect = document.querySelector("#btn-connect")!;

btnConnect.addEventListener("click", () => {
  if (jwtToken.value.trim().length <= 0) return;
  connectToServer(jwtToken.value.trim());
});
